class Door : ScriptObject
{
    // Player distance for the door to close automatically. Set to 0 to disable
    float autoCloseDistance = 10.0f;
    bool closed = true;
    bool _disablePending = false;

    Vector3 _stillPos;

    void Start()
    {
        SubscribeToEvent(node, "Use", "HandleActivated");
        SubscribeToEvent(node, "NodeCollision", "HandleNodeCollision");
        SubscribeToEvent(physicsWorld, "PhysicsPostStep", "HandlePhysics");
        _stillPos = node.position;
        // Mark the current node as interactive
        node.vars["selectable"] = true;
    }

    void HandleActivated(StringHash eventType, VariantMap& eventData)
    {
        log.Debug(node.name + ": Activated");
        RigidBody@ body = node.GetComponent("RigidBody");
        body.mass = 1.0f;
        if (closed)
        {
            // Pull the doorknob
            Node@ knob = node.GetChild("knob");
            Component@ knobScript = knob.GetComponent("ScriptInstance");
            knobScript.enabled = true;

            body.ApplyTorque(Vector3(0, -50.0f, 0));
            closed = false;

            // Setup player distance poll once a second for autoclose feature
            if (autoCloseDistance > 0)
            {
                DelayedExecute(1.0f, true, "void CheckPlayerDistance()");
            }
        }
        else
        {
            body.ApplyTorque(Vector3(0, 50.0f, 0));
        }
    }

    void HandleNodeCollision(StringHash eventType, VariantMap& eventData)
    {
        Node@ otherNode = eventData["OtherNode"].GetNode();
        RigidBody@ body = node.GetComponent("RigidBody");

        if (otherNode.name == "limiter"
            && body.angularVelocity.y > 0)
        {
            log.Debug(node.name + ": Closed");
            // Disable the door dynamics after the physics step
            _disablePending = true;

            node.rotation = Quaternion();
            node.position = _stillPos;

            // Stop autoclose polling
            ClearDelayedExecute();
        }
    }

    void HandlePhysics(StringHash eventType, VariantMap& eventData)
    {
        if(_disablePending)
        {
            RigidBody@ body = node.GetComponent("RigidBody");
            Constraint@ hinge = node.GetComponent("Constraint");
            // Disable the door dynamics
            body.mass = 0;
            _disablePending = false;
            closed = true;
        }
    }

    void CheckPlayerDistance()
    {
        // If the door was already closed - cancel polling
        if (closed) {
            ClearDelayedExecute();
            return;
        }

        // If the player is too far away - close the door
        Node@ player = scene.GetChild("player");
        if (player !is null)
        {
            Vector3 delta = node.position - player.position;
            if (delta.lengthSquared > autoCloseDistance * autoCloseDistance)
            {
                ClearDelayedExecute();
                node.SendEvent("Use");
            }
        }
    }
}
