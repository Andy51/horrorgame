class Lamp : ScriptObject
{
    Material@ activeMaterial = cache.GetResource("Material", "Materials/LightbulbActive.xml");
    Material@ inactiveMaterial = cache.GetResource("Material", "Materials/LightbulbInactive.xml");

    void Start()
    {
        SubscribeToEvent(node, "Activate", "HandleActivated");
    }

    void HandleActivated(StringHash eventType, VariantMap& eventData)
    {
        bool active = false;
        // Toggle all the lights
        Array<Component@>@ components = node.GetComponents("Light", true);
        for (uint i = 0; i < components.length; ++i)
        {
            Light@ light = components[i];
            light.enabled = !light.enabled;
            active = light.enabled;
        }

        if(components.length == 0)
        {
            log.Warning(node.name + ": No lights found!");
        }

        // Switch material on the StaticModel
        StaticModel@ lightbulb = node.GetComponent("StaticModel");
        if(lightbulb !is null)
        {
            Material@ newMaterial;
            if(active)
            {
                newMaterial = activeMaterial;
            }
            else
            {
                newMaterial = inactiveMaterial;
            }

            if(newMaterial !is null)
            {
                lightbulb.material = newMaterial;
            }
        }
        else
        {
            log.Warning(node.name + ": No StaticModel found!");
        }
    }
}
