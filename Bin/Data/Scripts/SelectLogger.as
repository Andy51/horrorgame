class SelectLogger : ScriptObject
{
    String testAttribute;

    void Start()
    {
        SubscribeToEvent(node, "Select", "HandleSelected");
        SubscribeToEvent(node, "Activate", "HandleActivated");
    }

    void HandleActivated(StringHash eventType, VariantMap& eventData)
    {
        Print("Activated " + node.name + " with attrib: " + testAttribute);
    }

    void HandleSelected(StringHash eventType, VariantMap& eventData)
    {
        Print("Selected " + node.name + " with attrib: " + testAttribute);
    }

    // Update is called during the variable timestep scene update
    void Update(float timeStep)
    {

    }
}
