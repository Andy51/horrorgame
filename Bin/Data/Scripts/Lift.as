#include "Scripts/Utilities/TranslationAnimator.as"

enum State
{
    eIdle,
    eMoving,
    eOpening,
    eOpened,
    eClosing,
}

funcdef State StateHandler(StringHash eventType, VariantMap& eventData);

// Doors open/close delays
const float _doorDelayIdle = 4.0f;
const float _doorDelayCalled = 2.0f;

// Timer settings
const float _timerGranularity = 1.0f;

class Lift : ScriptObject
{
    // Current floor
    int floor = 0;
    int _callPending = 0;
    float floorHeight = 5.0f;

    // Timer settings
    float _timeoutCounter = 0.0f;
    float _currentTimeout;

    // FSM vars
    StateHandler@ curStateHandler;
    State curState;

    void DelayedStart()
    {
        // Initialize FSM
        SubscribeToEvent(node, "Activate", "HandleEvent");
        SubscribeToEvent(node, "AnimationFinished", "HandleEvent");
        Node@ doors = node.GetChild("slidingDoors");
        SubscribeToEvent(doors, "AnimationFinished", "HandleEvent");
        SubscribeToEvent(node, "Timeout", "HandleEvent");

        ChangeState(eIdle);

        // Initialize TranslationAnimator
        TranslationAnimator@ animator = cast<TranslationAnimator>(node.GetScriptObject("TranslationAnimator"));
        animator.targetPosition = node.position;
    }

    void HandleActivated(StringHash eventType, VariantMap& eventData)
    {
        int arg = eventData["arg"].GetInt();
        Print("Activated with arg = " + arg);

        TranslationAnimator@ animator = cast<TranslationAnimator>(node.GetScriptObject("TranslationAnimator"));
        animator.GetInstance().enabled = true;
    }

    void ChangeState(State state)
    {
        switch(state)
        {
            case eIdle:
                curStateHandler = StateHandler(HandleInIdle);
                break;
            case eMoving:
                curStateHandler = StateHandler(HandleInMoving);
                break;
            case eOpening:
                curStateHandler = StateHandler(HandleInOpening);
                break;
            case eOpened:
                curStateHandler = StateHandler(HandleInOpened);
                break;
            case eClosing:
                curStateHandler = StateHandler(HandleInClosing);
                break;
        }
        Print("STATE " + curState + " -> " + state);
        curState = state;
    }

    void HandleEvent(StringHash eventType, VariantMap& eventData)
    {
        Print("Handle event " + eventType.ToString() + " in state " + curState);
        State result = curStateHandler(eventType, eventData);
        if(result != curState)
        {
            ChangeState(result);
        }
    }

    void MoveToFloor(int callFloor)
    {
        // Start moving
        TranslationAnimator@ animator = cast<TranslationAnimator>(node.GetScriptObject("TranslationAnimator"));
        animator.targetPosition.y = callFloor * floorHeight;
        animator.GetInstance().enabled = true;
        floor = callFloor;
    }

    void OpenCloseDoors()
    {
        // Get all the sliding doors in the scene
        Array<Node@>@ doorNodes = scene.GetChildrenWithScript("SlidingDoors", true);
        for (uint i=0; i < doorNodes.length; i++)
        {
            Node@ doorNode = doorNodes[i];
            Vector3 delta(doorNode.worldPosition - node.worldPosition);
            // Open all the sliding doors in the radius of 2 meters
            if (delta.lengthSquared < 4.0f)
            {
                doorNode.SendEvent("Activate");
            }
        }
    }

    bool HandleActivate(StringHash eventType, VariantMap& eventData)
    {
        if (eventType == StringHash("Activate"))
        { // Someone called the lift from inside or outside
            int callFloor = eventData["arg"].GetInt();
            _callPending = callFloor;
            return true;
        }

        return false;
    }

    State HandleInIdle(StringHash eventType, VariantMap& eventData)
    {
        if (HandleActivate(eventType, eventData))
        { // Someone called the lift from inside or outside
            if (floor != _callPending)
            { // Lift was called to another floor
                MoveToFloor(_callPending);
                return eMoving;
            }
            else
            { // Lift was called at the same floor
                // Open the doors
                OpenCloseDoors();
                return eOpening;
            }
        }
        return eIdle;
    }

    State HandleInMoving(StringHash eventType, VariantMap& eventData)
    {
        Node@ sender = GetEventSender();

        HandleActivate(eventType, eventData);

        if (eventType == StringHash("AnimationFinished")
            && sender.name == "lift")
        { // Lift arrived
            // Open the doors
            OpenCloseDoors();
            return eOpening;
        }
        return eMoving;
    }

    void TimerInit(float timeout)
    {
        TimerStop();
        _currentTimeout = timeout;
        DelayedExecute(_timerGranularity, true, "void TimerTick()");
    }

    void TimerTick()
    {
        _timeoutCounter += _timerGranularity;
        if (_timeoutCounter >= _currentTimeout)
        {
            TimerStop();
            node.SendEvent("Timeout");
        }
    }

    void TimerSet(float timeout)
    {
        if (_timeoutCounter >= timeout)
        {
            // Schedule a tick on the next update
            TimerInit(0);
        }
        else
        {
            _currentTimeout = timeout;
        }
    }

    void TimerStop()
    {
        ClearDelayedExecute();
        _timeoutCounter = 0;
    }

    State HandleInOpening(StringHash eventType, VariantMap& eventData)
    {
        Node@ sender = GetEventSender();

        HandleActivate(eventType, eventData);

        if (eventType == StringHash("AnimationFinished")
            && sender.name == "slidingDoors")
        { // Doors opened
            // Start door closing timer
            TimerInit((_callPending == floor) ? _doorDelayIdle : _doorDelayCalled);

            return eOpened;
        }
        return eOpening;
    }

    State HandleInOpened(StringHash eventType, VariantMap& eventData)
    {
        Node@ sender = GetEventSender();
        if (HandleActivate(eventType, eventData))
        { // Lift was called
            if (_callPending == floor)
            {
                // If called on the same floor - restart the timer
                TimerInit(_doorDelayIdle);
            }
            else
            {
                // Reduce the timeout (this handles multiple presses on the call button)
                TimerSet(_doorDelayCalled);
            }
        }
        else if (eventType == StringHash("Timeout"))
        { // Door timeout happened
            // Close the doors
            OpenCloseDoors();
            return eClosing;
        }

        return eOpened;
    }

    State HandleInClosing(StringHash eventType, VariantMap& eventData)
    {
        HandleActivate(eventType, eventData);

        Node@ sender = GetEventSender();

        if (eventType == StringHash("AnimationFinished")
            && sender.name == "slidingDoors")
        { // Doors closed
            if (floor != _callPending)
            { // Lift was called to another floor
                MoveToFloor(_callPending);
                return eMoving;
            }
            else
            { // Lift was not called (or called at the same floor)
                // Just go to idle
                return eIdle;
            }
        }
        return eClosing;
    }

}
