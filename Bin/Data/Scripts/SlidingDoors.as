class SlidingDoors : ScriptObject
{
    bool closed = true;

    void Start()
    {
        SubscribeToEvent(node, "Activate", "HandleActivated");
    }

    void HandleActivated(StringHash eventType, VariantMap& eventData)
    {
        log.Debug(node.name + ": Activated");
        // Start the child animations for left and right door halves
        Node@ doorHalve = node.GetChild("left");
        ScriptInstance@ instance = cast<ScriptInstance>(doorHalve.GetComponent("ScriptInstance"));
        instance.enabled = true;

        doorHalve = node.GetChild("right");
        instance = cast<ScriptInstance>(doorHalve.GetComponent("ScriptInstance"));
        instance.enabled = true;

        // Subscribe to AnimationFinished to propagate it from this node
        SubscribeToEvent(doorHalve, "AnimationFinished", "HandleAnimationFinished");
    }

    void HandleAnimationFinished(StringHash eventType, VariantMap& eventData)
    {
        closed = !closed;

        // Propagate the animation finished event
        Print("HandleAnimationFinished");
        node.SendEvent("AnimationFinished");
    }

}
