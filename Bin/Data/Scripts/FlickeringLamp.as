class FlickeringLamp : ScriptObject
{
    Vector2 intensityLowHigh(0.3, 1.2f);
    Vector2 burstPause(0.5f, 1.7f);
    Vector2 burstDelay(0.03f, 0.3f);
    IntVector2 burstLength(2, 7);
    Material@ hiMaterial = cache.GetResource("Material", "Materials/LightbulbActive.xml");
    Material@ lowMaterial = cache.GetResource("Material", "Materials/LightbulbLow.xml");

    Sound@ hummSound = cache.GetResource("Sound", "Sounds/LampHumm.wav");
    Sound@ bingSound = cache.GetResource("Sound", "Sounds/LampBing.wav");

    bool _isLowPhase = false;
    int _burstCounter = 0;
    int _curBurstLength = 0;

    void Start()
    {
        // We should clear all the delayed calls if scheduling in Start()
        ClearDelayedExecute();
        RandomTimestep();
    }

    void RandomTimestep()
    {
        float delay;
        if (_burstCounter == _curBurstLength)
        {
            // Last burst finished - create a long delay
            delay = Random(burstPause.x, burstPause.y);
            // and schedule the next burst
            _curBurstLength = RandomInt(burstLength.x, burstLength.y);
            _burstCounter = 0;
        }
        else
        {
            // Short delay in a burst
            delay = Random(burstDelay.x, burstDelay.y);
        }

        _burstCounter++;
        DelayedExecute(delay, false, "void Flicker()");
    }

    void Flicker()
    {
        float intensity = intensityLowHigh.x;
        if (!_isLowPhase)
            intensity = intensityLowHigh.y;

        Color curColor = Color(1.0f, 1.0f, 1.0f) * intensity;

        // Toggle all the lights
        Array<Component@>@ components = node.GetComponents("Light", true);
        for (uint i = 0; i < components.length; ++i)
        {
            Light@ light = components[i];
            light.color = curColor;
        }

        if(components.length == 0)
        {
            log.Warning(node.name + ": No lights found!");
        }

        // Switch material on the StaticModel
        StaticModel@ lightbulb = node.GetComponent("StaticModel");
        if(lightbulb !is null)
        {
            Material@ newMaterial;
            if(_isLowPhase)
            {
                newMaterial = lowMaterial;
            }
            else
            {
                newMaterial = hiMaterial;
            }

            if(newMaterial !is null)
            {
                lightbulb.material = newMaterial;
            }
        }
        else
        {
            log.Warning(node.name + ": No StaticModel found!");
        }

        // Play a sound
        Sound@ sound = hummSound;
        if (!_isLowPhase)
            sound = bingSound;
        SoundSource3D@ soundSource = node.GetComponent("SoundSource3D");
        soundSource.Play(sound);

        _isLowPhase = !_isLowPhase;

        RandomTimestep();
    }
}
