class RotationAnimator : ScriptObject
{
    // The orientation to turn the node to
    Quaternion targetRotation;
    // Duration of the animation, seconds
    float duration;
    // Animation reverted back and forth after finishing an iteration
    bool pingpong = false;
    // Repetitions count, 0 - indefinite
    int iterations = 0;

    Quaternion _initRotation;
    float _runningTime = 0;
    int _iterationsCounter;

    void Start()
    {
        //ApplyAttributes();
    }

    void ApplyAttributes()
    {
        Print("apply");
        ResetIteration();
        _iterationsCounter = iterations;
    }

    void ResetIteration()
    {
        _initRotation = node.rotation;
        _runningTime = 0;
    }

    // Update is called during the variable timestep scene update
    void Update(float timeStep)
    {
        _runningTime += timeStep;

        if (_runningTime > duration)
        {
            _runningTime = duration;
        }

        float fraction = _runningTime / duration;

        node.rotation = _initRotation.Slerp(targetRotation, fraction);

        if (fraction == 1.0f)
        {
            if (pingpong)
            {
                targetRotation = _initRotation;
            }
            else
            {
                node.rotation = _initRotation;
            }
            ResetIteration();

            if(--_iterationsCounter == 0)
            {
                _iterationsCounter = iterations;
                self.enabled = false;
            }
        }
    }
}
