class BlinkAnimator : ScriptObject
{
    // Duration of the object visibility
    float durationOn = 1.0f;
    // Duration of the object invisibility
    float durationOff = 1.0f;
    // Repetitions count, 0 - indefinite
    int iterations = 0;
    bool isOn = true;

    float _elapsed;
    int _iterationsCounter;

    void Start()
    {
        float curDuration = isOn ? durationOn : durationOff;
        // We should clear all the delayed calls if scheduling in Start()
        ClearDelayedExecute();
        DelayedExecute(curDuration, false, "void Timeout()");
    }

    void ApplyAttributes()
    {
        _elapsed = 0.0f;
        _iterationsCounter = iterations;
    }

    void Timeout()
    {
        float curDuration = isOn ? durationOn : durationOff;
        DelayedExecute(curDuration, false, "void Timeout()");

        // Switch all the components in the same node except self
        Array<Component@>@ comps = Array<Component@>(node.GetComponents());
        for (uint i=0; i < comps.length; i++)
        {
            Component@ comp = comps[i];
            if (comp !is self)
                comp.enabled = !comp.enabled;
        }

        isOn = !isOn;

        if (--_iterationsCounter == 0)
        {
            _iterationsCounter = iterations;
            self.enabled = false;
        }


    }
}
