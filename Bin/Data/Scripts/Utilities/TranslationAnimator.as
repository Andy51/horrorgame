class TranslationAnimator : ScriptObject
{
    // The orientation to turn the node to
    Vector3 targetPosition;
    // Duration of the animation, seconds
    float duration;
    // Animation reverted back and forth after finishing an iteration
    bool pingpong = false;
    // Repetitions count, 0 - indefinite
    int iterations = 0;

    Vector3 _initPosition;
    float _runningTime = 0;
    int _iterationsCounter;

    void Start()
    {
    }

    ScriptInstance@ GetInstance()
    {
        return self;
    }

    void ApplyAttributes()
    {
        ResetIteration();
        _iterationsCounter = iterations;
    }

    void ResetIteration()
    {
        _initPosition = node.position;
        _runningTime = 0;
    }

    // Update is called during the variable timestep scene update
    void FixedUpdate(float timeStep)
    {
        _runningTime += timeStep;

        if (_runningTime > duration)
        {
            _runningTime = duration;
        }

        float fraction = _runningTime / duration;

        node.position = _initPosition.Lerp(targetPosition, fraction);

        if (fraction == 1.0f)
        {
            if (pingpong)
            {
                targetPosition = _initPosition;
            }
            else
            {
                node.position = _initPosition;
            }
            ResetIteration();

            if(--_iterationsCounter == 0)
            {
                // Animation finished, reset the iteration counter
                _iterationsCounter = iterations;
                // Notify the node that the animation finished
                node.SendEvent("AnimationFinished");
                // Stop the script updates
                self.enabled = false;
            }
        }
    }
}
