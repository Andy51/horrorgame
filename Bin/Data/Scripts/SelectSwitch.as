class SelectSwitch : ScriptObject
{
    String what;
    int argument = 0;

    VariantMap _arguments;

    void Start()
    {
        SubscribeToEvent(node, "Use", "HandleUsed");
        // Mark the current node as interactive
        node.vars["selectable"] = true;
    }

    void ApplyAttributes()
    {
        Print("SelectSwitch: apply");
        _arguments["arg"] = argument;
    }

    void HandleUsed(StringHash eventType, VariantMap& eventData)
    {
        Node@ target = scene.GetChild(what, true);
        if (target !is null)
        {
            target.SendEvent("Activate", _arguments);
        }
        else
        {
            log.Error(node.name + ": Target not found! what=" + what);
        }
    }
}
