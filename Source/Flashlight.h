#pragma once

#include <Component.h>
#include <Controls.h>

using namespace Urho3D;

namespace Urho3D
{
}

/// Flashlight item
class Flashlight : public Component
{
    OBJECT(Flashlight)

public:
    /// Construct.
    Flashlight(Context* context);
    
    /// Register object factory and attributes.
    static void RegisterObject(Context* context);
    
    /// Handle node being assigned.
    virtual void OnNodeSet(Node* node);
    
    static Flashlight* populateNode(Node* objectNode);

};
