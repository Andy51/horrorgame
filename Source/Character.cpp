//
// Copyright (c) 2008-2013 the Urho3D project.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <AnimatedModel.h>
#include <AnimationController.h>
#include <Audio.h>
#include <BorderImage.h>
#include <Camera.h>
#include <CollisionShape.h>
#include <Context.h>
#include <Font.h>
#include <Graphics.h>
#include <Log.h>
#include <Material.h>
#include <MemoryBuffer.h>
#include <PhysicsEvents.h>
#include <PhysicsWorld.h>
#include <ResourceCache.h>
#include <RigidBody.h>
#include <Scene.h>
#include <Sprite.h>
#include <SceneEvents.h>
#include <SoundListener.h>
#include <Texture2D.h>
#include <Text.h>
#include <UI.h>

#include "Flashlight.h"
#include "Character.h"

const Color SELECTION_EMISSIVE(0.2f, 0.2f, 0.0f, 1.0f);
const float SELECTION_DISTANCE = 3.0f;

Character::Character(Context* context) :
    Component(context),
    onGround_(false),
    okToJump_(true),
    inAirTimer_(0.0f),
    mSavedSelectionMaterial(NULL)
{

}

void Character::RegisterObject(Context* context)
{
    context->RegisterFactory<Character>();

    // These macros register the class attributes to the Context for automatic load / save handling.
    // We specify the Default attribute mode which means it will be used both for saving into file, and network replication
    ATTRIBUTE(Character, VAR_FLOAT, "Controls Yaw", controls_.yaw_, 0.0f, AM_DEFAULT);
    ATTRIBUTE(Character, VAR_FLOAT, "Controls Pitch", controls_.pitch_, 0.0f, AM_DEFAULT);
    ATTRIBUTE(Character, VAR_BOOL, "On Ground", onGround_, false, AM_DEFAULT);
    ATTRIBUTE(Character, VAR_BOOL, "OK To Jump", okToJump_, true, AM_DEFAULT);
    ATTRIBUTE(Character, VAR_FLOAT, "In Air Timer", inAirTimer_, 0.0f, AM_DEFAULT);
}

void Character::OnNodeSet(Node* node)
{
    if (node)
    {
        // Component has been inserted into its scene node. Subscribe to events now
        SubscribeToEvent(node, E_NODECOLLISION, HANDLER(Character, HandleNodeCollision));
        SubscribeToEvent(GetScene()->GetComponent<PhysicsWorld>(), E_PHYSICSPRESTEP, HANDLER(Character, HandleFixedUpdate));

        CreateHUD();
    }
}

void Character::CreateHUD()
{
    UI* ui = GetSubsystem<UI>();
    UIElement* root = ui->GetRoot();
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    // Create 'Press <key> to use' notification

    mUseNotificationText = new Text(context_);
    mUseNotificationText->SetColor(Color(1.0f, 1.0f, 1.0f, 1.0f));
    mUseNotificationText->SetHorizontalAlignment(HA_LEFT);
    mUseNotificationText->SetVerticalAlignment(VA_TOP);
    mUseNotificationText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);
    mUseNotificationText->SetText("Press E to use");
    mUseNotificationText->SetPosition(50, 50);
    mUseNotificationText->SetVisible(false);
    root->AddChild(mUseNotificationText);

    // Create a crosshair

    mCrosshair = new BorderImage(context_);
    Texture* crosshairTex = cache->GetResource<Texture2D>("Textures/Crosshair.png");
    mCrosshair->SetTexture(crosshairTex);
    mCrosshair->SetAlignment(HA_CENTER, VA_CENTER);
    // Resize the crosshair for various resolutions. Initially it is 9x9 for 1080p - thus 1/120 for downscale
    Graphics* graphics = GetSubsystem<Graphics>();
    int height = graphics->GetHeight() / 120;
    // Do not upscale
    if (height > crosshairTex->GetHeight())
        height = crosshairTex->GetHeight();
    mCrosshair->SetSize(height, height);
    mCrosshair->SetBlendMode(BLEND_ADDALPHA);
    mCrosshair->SetVisible(false);
    root->AddChild(mCrosshair);
}

void Character::ShowSelectionUI(bool show)
{
    mCrosshair->SetVisible(show);
    mUseNotificationText->SetVisible(show);
}

void Character::HandleNodeCollision(StringHash eventType, VariantMap& eventData)
{
    // Check collision contacts and see if character is standing on ground (look for a contact that has near vertical normal)
    using namespace NodeCollision;

    MemoryBuffer contacts(eventData[P_CONTACTS].GetBuffer());

    while (!contacts.IsEof())
    {
        Vector3 contactPosition = contacts.ReadVector3();
        Vector3 contactNormal = contacts.ReadVector3();
        float contactDistance = contacts.ReadFloat();
        float contactImpulse = contacts.ReadFloat();

        // If contact is below node center and mostly vertical, assume it's a ground contact
        if (contactPosition.y_ < (node_->GetPosition().y_ + 1.0f))
        {
            float level = Abs(contactNormal.y_);
            if (level > 0.75)
                onGround_ = true;
        }
    }
}

void Character::HandleFixedUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace PhysicsPreStep;

    float timeStep = eventData[P_TIMESTEP].GetFloat();

    /// \todo Could cache the components for faster access instead of finding them each frame
    RigidBody* body = GetComponent<RigidBody>();

    // Update the in air timer. Reset if grounded
    if (!onGround_)
        inAirTimer_ += timeStep;
    else
        inAirTimer_ = 0.0f;
    // When character has been in air less than 1/10 second, it's still interpreted as being on ground
    bool softGrounded = inAirTimer_ < INAIR_THRESHOLD_TIME;

    // Update movement & animation
    const Quaternion& rot = node_->GetRotation();
    Vector3 moveDir = Vector3::ZERO;
    const Vector3& velocity = body->GetLinearVelocity();
    // Velocity on the XZ plane
    Vector3 planeVelocity(velocity.x_, 0.0f, velocity.z_);

    if (controls_.IsDown(CTRL_FORWARD))
        moveDir += Vector3::FORWARD;
    if (controls_.IsDown(CTRL_BACK))
        moveDir += Vector3::BACK;
    if (controls_.IsDown(CTRL_LEFT))
        moveDir += Vector3::LEFT;
    if (controls_.IsDown(CTRL_RIGHT))
        moveDir += Vector3::RIGHT;

    // Normalize move vector so that diagonal strafing is not faster
    if (moveDir.LengthSquared() > 0.0f)
        moveDir.Normalize();

    // If in air, allow control, but slower than when on ground
    body->ApplyImpulse(rot * moveDir * (softGrounded ? MOVE_FORCE : INAIR_MOVE_FORCE));

    if (softGrounded)
    {
        // When on ground, apply a braking force to limit maximum ground velocity
        Vector3 brakeForce = -planeVelocity * BRAKE_FORCE;
        body->ApplyImpulse(brakeForce);

        // Jump. Must release jump control inbetween jumps
        if (controls_.IsDown(CTRL_JUMP))
        {
            if (okToJump_)
            {
                body->ApplyImpulse(Vector3::UP * JUMP_FORCE);
                okToJump_ = false;
            }
        }
        else
            okToJump_ = true;
    }

    // Reset grounded flag for next frame
    onGround_ = false;

    if (controls_.IsPressed(CTRL_USE, lastControls))
    {
        ActivateSelected();
    }

    lastControls = controls_;
}

void Character::OnControlsUpdated()
{
    GetNode()->SetRotation(Quaternion(controls_.yaw_, Vector3::UP));
    camNode->SetRotation(Quaternion(controls_.pitch_, Vector3::RIGHT));
    CheckTarget();
}

void Character::RestoreSelectionMaterial()
{
    if (mSavedSelectionMaterial != NULL)
    {
        StaticModel *model = mSelectedNode->GetComponent<StaticModel>();
        if (model != NULL)
            model->SetMaterial(mSavedSelectionMaterial);
        mSavedSelectionMaterial = NULL;
    }
}

bool Character::CheckTarget()
{
    PhysicsWorld* pw = node_->GetScene()->GetComponent<PhysicsWorld>();
    PhysicsRaycastResult r;

    Vector3 origin = camNode->GetWorldPosition() + camNode->GetWorldRotation() * Vector3(0, 0, 0.15f);
    Vector3 dir = camNode->GetWorldRotation() * Vector3(0, 0, 1.0f);

    pw->RaycastSingle(r, Ray(origin, dir), SELECTION_DISTANCE);

    if (r.body_ != NULL)
    {
        Node *node = r.body_->GetNode();

        if (node != mSelectedNode)
        {
            if (node->GetVar("selectable").GetBool())
            {
                // Restore the initial material to previously selected model, if any
                RestoreSelectionMaterial();

                StaticModel *model = node->GetComponent<StaticModel>();
                if (model != NULL)
                {
                    Material *material = model->GetMaterial();
                    if (material != NULL)
                    {
                        mSavedSelectionMaterial = material;
                        mSelectionMaterial = material->Clone();

                        mSelectionMaterial->SetShaderParameter("MatEmissiveColor", SELECTION_EMISSIVE);
                        model->SetMaterial(mSelectionMaterial);
                    }
                }

                ShowSelectionUI(true);

                mSelectedNode = node;
                mSelectedNode->SendEvent("Select");
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    // Selection removed

    // Restore the initial material to previously selected model, if any
    RestoreSelectionMaterial();
    mSelectedNode = NULL;

    ShowSelectionUI(false);

    return false;
}

bool Character::ActivateSelected()
{
    if (mSelectedNode != NULL)
    {
        mSelectedNode->SendEvent("Use");
        return true;
    }
    return false;
}

Character* Character::populateNode(Node* objectNode)
{
    // Player hierarchy
    // + root - defines both player's body and head (camera) yaw
    // |-- body - rigid body for physics, defines position
    // |-- shape - collision shape
    // |--+ headPitch - defines head pitch
    // |  \-- Camera
    // \--+ wieldNode - item in hands mountpoint (TODO)

    ResourceCache* cache = objectNode->GetSubsystem<ResourceCache>();

    // Camera pitch control node
    Node* camNode = objectNode->CreateChild("headPitch");
    camNode->Translate(Vector3::UP * PLAYER_HEIGHT);
    Camera *camera = camNode->CreateComponent<Camera>();
    camera->SetFarClip(300.0f);

    // Create rigidbody, and set non-zero mass so that the body becomes dynamic
    RigidBody* body = objectNode->CreateComponent<RigidBody>();
    body->SetCollisionLayer(1);
    body->SetMass(1.0f);

    // Set zero angular factor so that physics doesn't turn the character on its own.
    // Instead we will control the character yaw manually
    body->SetAngularFactor(Vector3::ZERO);

    // Set the rigidbody to signal collision also when in rest, so that we get ground collisions properly
    body->SetCollisionEventMode(COLLISION_ALWAYS);

    // Set a capsule shape for collision
    CollisionShape* shape = objectNode->CreateComponent<CollisionShape>();
    shape->SetCapsule(PLAYER_WIDTH, PLAYER_HEIGHT, Vector3(0.0f, PLAYER_HEIGHT / 2, 0.0f));

    // Wielded items node
    Node* wieldNode = camNode->CreateChild("wield"); // TODO: move to root node
    wieldNode->Translate(Vector3(PLAYER_WIDTH * 0.3f, PLAYER_HEIGHT * -0.1f, PLAYER_WIDTH * 0.5f));

    Flashlight::populateNode(wieldNode);

    // Create the character logic component, which takes care of steering the rigidbody
    Character *character = objectNode->CreateComponent<Character>();
    character->camNode = camNode;
    character->camera_ = camera;

    // Initialize the orientation from node transformations
    Vector3 euler(objectNode->GetRotation().EulerAngles());
    character->controls_.yaw_ = euler.y_;
    character->controls_.pitch_ = euler.x_;

    // Add a sound listener for the player
    SoundListener *listener = objectNode->CreateComponent<SoundListener>();
    Audio* audio = objectNode->GetSubsystem<Audio>();
    audio->SetListener(listener);

    return character;
}
