#include <AnimatedModel.h>
#include <AnimationController.h>
#include <Camera.h>
#include <CollisionShape.h>
#include <Context.h>
#include <Material.h>
#include <MemoryBuffer.h>
#include <PhysicsEvents.h>
#include <PhysicsWorld.h>
#include <ResourceCache.h>
#include <RigidBody.h>
#include <Scene.h>
#include <SceneEvents.h>
#include <XMLFile.h>

#include "Flashlight.h"

Flashlight::Flashlight(Context* context) :
    Component(context)
{
}

void Flashlight::RegisterObject(Context* context)
{
    context->RegisterFactory<Flashlight>();
}

void Flashlight::OnNodeSet(Node* node)
{
}

Flashlight* Flashlight::populateNode(Node* objectNode)
{
    ResourceCache* cache = objectNode->GetSubsystem<ResourceCache>();

    // Preserve the original node transformation
    Vector3 savedPos(objectNode->GetPosition());
    Quaternion savedRot(objectNode->GetRotation());

    // Load the static part from prefab
    XMLFile* xml = cache->GetResource<XMLFile>("Objects/Flashlight.xml");
    objectNode->LoadXML(xml->GetRoot());

    // Restore the transformation
    objectNode->SetTransform(savedPos, savedRot);

    // Create the Flashlight logic component
    Flashlight *flashlight = objectNode->CreateComponent<Flashlight>();

    return flashlight;
}
