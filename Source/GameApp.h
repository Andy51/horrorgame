#pragma once

#include <Application.h>

class Character;

namespace Urho3D
{
class Node;
class Scene;
}

// All Urho3D classes reside in namespace Urho3D
using namespace Urho3D;

const float INIT_CAM_HEIGHT = 3.0f;

class GameApp : public Application
{
    // Enable type information.
    OBJECT(GameApp);

public:
    /// Construct.
    GameApp(Context* context);

    /// Setup before engine initialization. Modifies the engine parameters.
    virtual void Setup();
    /// Setup after engine initialization. Creates the logo, console & debug HUD.
    virtual void Start();

private:

    void CreateScene();
    void MoveCamera(float timeStep);
    void SetFreeCamera(bool enable);

    /// Handle key down event to process key controls common to all samples.
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);

    SharedPtr<Scene> scene;
    SharedPtr<Node> cameraNode;
    Character* player;

    /// Camera yaw angle.
    float yaw_;
    /// Camera pitch angle.
    float pitch_;

    bool freeCam;
};

