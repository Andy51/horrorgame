
#include <Application.h>
#include <Camera.h>
#include <CoreEvents.h>
#include <CollisionShape.h>
#include <Console.h>
#include <Engine.h>
#include <FileSystem.h>
#include <Input.h>
#include <InputEvents.h>
#include <Light.h>
#include <Material.h>
#include <Model.h>
#include <Octree.h>
#include <PhysicsWorld.h>
#include <Renderer.h>
#include <ResourceCache.h>
#include <RigidBody.h>
#include <Scene.h>
#include <Script.h>
#include <StaticModel.h>
#include <UI.h>
#include <Zone.h>

#include "GameApp.h"
#include "Character.h"

GameApp::GameApp(Context* context) :
    Application(context)
{
    Character::RegisterObject(context);
    context_->RegisterSubsystem(new Script(context_));
}

void GameApp::Setup()
{
    // Modify engine startup parameters
    engineParameters_["WindowTitle"] = GetTypeName();
    engineParameters_["LogName"]     = GetTypeName() + ".log";
    engineParameters_["FullScreen"]  = false;
    engineParameters_["Headless"]    = false;
}

void GameApp::Start()
{
    CreateScene();

    // Create console
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* xmlFile = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
    Console* console = engine_->CreateConsole();
    console->SetDefaultStyle(xmlFile);

    // Subscribe key down event
    SubscribeToEvent(E_KEYDOWN, HANDLER(GameApp, HandleKeyDown));
    // Subscribe to Update event
    SubscribeToEvent(E_UPDATE, HANDLER(GameApp, HandleUpdate));
}

void GameApp::CreateScene()
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    scene = new Scene(context_);

    // Load the test scene from XML
    File loadFile(context_, GetSubsystem<FileSystem>()->GetProgramDir() + "Data/Scenes/TestScene.xml", FILE_READ);
    scene->LoadXML(loadFile);

    // Create a global camera and define viewport. We will be doing load / save, so it's convenient to create the camera outside the scene,
    // so that it won't be destroyed and recreated, and we don't have to redefine the viewport on load
    cameraNode = new Node(context_);
    Camera* camera = cameraNode->CreateComponent<Camera>();
    camera->SetFarClip(300.0f);
    cameraNode->Translate(Vector3::UP * INIT_CAM_HEIGHT);
    GetSubsystem<Renderer>()->SetViewport(0, new Viewport(context_, scene, camera));

    // Place the player
    Node* charNode = scene->GetChild("player");
    if (charNode == NULL) {
        charNode = scene->CreateChild("player");
        charNode->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
    }
    player = Character::populateNode(charNode);

    SetFreeCamera(false);
}


void GameApp::MoveCamera(float timeStep)
{
    // Do not move if the UI has a focused element (the console)
//    if (GetSubsystem<UI>()->GetFocusElement())
//        return;

    Input* input = GetSubsystem<Input>();

    // Movement speed as world units per second
    const float MOVE_SPEED = 20.0f;
    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

    // Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
    cameraNode->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
    if (input->GetKeyDown('W'))
        cameraNode->TranslateRelative(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown('S'))
        cameraNode->TranslateRelative(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown('A'))
        cameraNode->TranslateRelative(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown('D'))
        cameraNode->TranslateRelative(Vector3::RIGHT * MOVE_SPEED * timeStep);
}

void GameApp::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();
    Input* input = GetSubsystem<Input>();

    if (freeCam)
    {
        MoveCamera(timeStep);
    }
    else
    {
        if (player)
        {
            UI* ui = GetSubsystem<UI>();

            // Get movement controls and assign them to the character logic component. If UI has a focused element, clear controls
            if (true)//!ui->GetFocusElement())
            {
                player->controls_.Set(CTRL_FORWARD, input->GetKeyDown('W'));
                player->controls_.Set(CTRL_BACK, input->GetKeyDown('S'));
                player->controls_.Set(CTRL_LEFT, input->GetKeyDown('A'));
                player->controls_.Set(CTRL_RIGHT, input->GetKeyDown('D'));
                player->controls_.Set(CTRL_JUMP, input->GetKeyDown(KEY_SPACE));
                player->controls_.Set(CTRL_USE, input->GetKeyDown('E'));

                // Add character yaw & pitch from the mouse motion
                player->controls_.yaw_ += (float)input->GetMouseMoveX() * YAW_SENSITIVITY;
                player->controls_.pitch_ += (float)input->GetMouseMoveY() * YAW_SENSITIVITY;
                // Limit pitch
                player->controls_.pitch_ = Clamp(player->controls_.pitch_, -80.0f, 80.0f);
            }
            else
                player->controls_.Set(CTRL_FORWARD | CTRL_BACK | CTRL_LEFT | CTRL_RIGHT | CTRL_JUMP | CTRL_USE, false);

            // Set rotation already here so that it's updated every rendering frame instead of every physics frame
            player->OnControlsUpdated();

        }
    }

}

void GameApp::SetFreeCamera(bool enable)
{
    if (enable)
    {
        // Set the global camera as main
        GetSubsystem<Renderer>()->GetViewport(0)->SetCamera(cameraNode->GetComponent<Camera>());
    }
    else
    {
        // Set the player's camera as main
        GetSubsystem<Renderer>()->GetViewport(0)->SetCamera(player->camera_);
    }

    freeCam = enable;
}

void GameApp::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
    using namespace KeyDown;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESC)
    {
        engine_->Exit();
    }

    if (key == KEY_TAB)
    {
        SetFreeCamera(!freeCam);
    }

    if (key == KEY_F1)
    {
        GetSubsystem<Console>()->Toggle();
    }

    // Scene saving
    if (key == KEY_F5)
    {
        File saveFile(context_, GetSubsystem<FileSystem>()->GetProgramDir() + "Data/Scenes/Saved.xml", FILE_WRITE);
        scene->SaveXML(saveFile);
    }
}

DEFINE_APPLICATION_MAIN(GameApp)
