@echo off
pushd %~dp0
cmake -E make_directory Build
set "arch="
set "version=9 2008"
if exist Build\CMakeCache.txt. for /F "eol=/ delims=:= tokens=1-3" %%i in (Build\CMakeCache.txt) do if "%%i" == "ENABLE_64BIT" if "%%k" == "1" set "arch= Win64"
:loop
if not "%1" == "" (
    if "%1" == "-DENABLE_64BIT" if "%~2" == "1" set "arch= Win64"
    if "%1" == "-DENABLE_64BIT" if "%~2" == "0" set "arch="
    if "%1" == "VERSION" set "version=%~2"
    shift
    shift
    goto loop
)
echo on
:: \todo suppress policy warning (for 2.8.12 early adopters), remove this option when CMake minimum version is 2.8.12
set "OPT=-Wno-dev"
cmake -E chdir Build cmake %OPT% -G "Visual Studio %version%%arch%" %* ..\Source
@popd